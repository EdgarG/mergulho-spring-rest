package com.wchua.logistic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLogisticApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLogisticApiApplication.class, args);
	}

}
