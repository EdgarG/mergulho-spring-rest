package com.wchua.logistic.domain.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
//@Table(name = "CLIENT")
public record Client(Long id, String name, String email, String phone) {

}
