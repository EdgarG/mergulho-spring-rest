package com.wchua.logistic.api.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wchua.logistic.domain.model.Client;

@RestController
class ClientController {

	@GetMapping("/clients")
	public List<Client> findAll(){
		var clients = List.of( 
				new Client(1L, "Client 1", "c1@mail.com", "1000-01"),
				new Client(2L, "Client 2", "c1@mail.com", "1000-02"),
				new Client(3L, "Client 3", "c1@mail.com", "1000-03"),
				new Client(4L, "Client 4", "c1@mail.com", "1000-04"));
		
		return clients;
	}
	
}
